const express = require("express");

const router = express.Router();

router.get("/vouchers", (request, response)=> {
    response.status(200).json({
        message: "GET All voucher"
    })
});

router.get('/vouchers/:voucherId', (request, response)=> {
    let voucherId = request.params.voucherId;
    response.status(200).json({

        message: "GET voucher Id = " + voucherId
    })
});

router.post("/vouchers", (request, response)=> {
    response.status(200).json({
        message: "Create voucher"
    })
});

router.put('/vouchers/:voucherId', (request, response)=> {
    let voucherId = request.params.voucherId;
    response.status(200).json({

        message: "Update voucher Id = " + voucherId
    })
});

router.delete('/vouchers/:voucherId', (request, response)=> {
    let voucherId = request.params.voucherId;
    response.status(200).json({

        message: "delete voucher Id = " + voucherId
    })
});

module.exports = router;